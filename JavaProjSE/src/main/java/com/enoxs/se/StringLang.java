package com.enoxs.se;

import org.junit.jupiter.api.Test;

public class StringLang {

    @Test
    public void runDeclareString(){
        String text = "Hello World";
        int length = text.length();
        char index0 = text.charAt(0);
        String upperCaseText = text.toUpperCase();
        char [] msg = text.toCharArray();
        System.out.println("text : " + text);
        System.out.println("length : " + length);
        System.out.println("index[0] : " + index0);
        System.out.println("upper case : " + upperCaseText);

        System.out.print("Char[] : ");
        for(char c : msg){
            System.out.print( c );
            System.out.print("  ");
        }
        System.out.println();

        String message = new String(msg);
        System.out.println("Merge : " + message);
    }

    @Test
    public void runConvertString2Var(){
        byte b = Byte.parseByte("100");
        short s = Short.parseShort("100");
        int n = Integer.parseInt("100");
        long l = Long.parseLong("100");
        float f = Float.parseFloat("100.0");
        double d = Double.parseDouble("100.0");

        System.out.println(b);
        System.out.println(s);
        System.out.println(n);
        System.out.println(l);
        System.out.println(f);
        System.out.println(d);
    }

    @Test
    public void compareStr(){
        String hello01 = "Hello";
        String hello02 = "Hello";
        System.out.println(hello01 == hello02);

        String hello03 = "Hel" + "lo";
        System.out.println(hello01 == hello03);

        char [] hello = {'H' , 'e' , 'l' , 'l' , 'o'};
        hello01 = new String(hello);
        hello02 = new String(hello);
        System.out.println(hello01 == hello02);
        System.out.println(hello01.equals(hello02));
    }

    @Test
    public void runStringBuffer(){
        StringBuffer sb = new StringBuffer(32);

        for(int i=0;i<10; i++){
            sb.append( i+1 );
            if(i < 9){
                sb.append(" , ");
            }
        }

        System.out.println(sb.toString());
    }

    @Test
    public void runStringBuilder(){
        StringBuilder sb = new StringBuilder(32);

        for(int i=0;i<10; i++){
            sb.append( i+1 );
            if(i < 9){
                sb.append(" , ");
            }
        }

        System.out.println(sb.toString());
    }

}
