package com.enoxs.basic;

import org.junit.jupiter.api.Test;

public class J2_FlowControl {


    @Test
    public void ctrlIfElse() {
        int num = 15;

        if (num < 5) {
            System.out.println(" num < 5");
        } else if (num > 5 && num < 10) {
            System.out.println(" 5 < num < 10");
        } else {
            System.out.println("num > 10");
        }
    }

    @Test
    public void ctrlSwitch() {
        int score = 88;
        int range = score / 10;
        char level;

        switch (range) {
            case 10:
            case 9:
                level = 'A';
                break;
            case 8:
                level = 'B';
                break;
            case 7:
                level = 'C';
                break;
            case 6:
                level = 'D';
                break;
            default:
                level = 'E';
        }
        System.out.println("Level => " + level);

    }

    @Test
    public void ctrlForOne2Ten() {
        for (int i = 0; i < 10; i++) {
            System.out.println("count => " + (i + 1));
        }
    }

    @Test
    public void ctrlForNineNineTable() {
        for (int i = 1; i < 10; i++) {
            for (int j = 1; j < 10; j++) {
                System.out.printf("%d*%d =%2d  ", j, i, j * i);
            }
            System.out.println();
        }
    }

    @Test
    public void crtlWhile01() {
        int count = 1;
        while (count < 20) {
            System.out.println("count => " + count);
            count++;
        }
    }

    @Test
    public void crtlWhile02() {
        int count = 1;
        while (count < 20) {
            System.out.println("count => " + count);
            if (count == 10) {
                break;
            }
            count++;
        }
    }

    @Test
    public void ctrlDoWhite() {
        int count = 1;
        do {
            System.out.println("count => " + count);
            count++;
        } while (count < 20);
    }

    @Test
    public void ctrlBreak() {
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                break;
            }
            System.out.println("count => " + (i + 1));
        }
    }

    @Test
    public void ctrlContinue() {
        for (int i = 0; i < 10; i++) {
            if (i == 5) {
                continue;
            }
            System.out.println("count => " + (i + 1));
        }
    }

    @Test
    public void ctrlBreakTag() {
        tag:
        {
            for (int i = 0; i < 10; i++) {
                if (i == 5) {
                    System.out.println("break");
                    break tag;
                }
                System.out.println("count => " + (i + 1));
            }
            System.out.printf("message");
        }
    }

    @Test
    public void ctrlContinueTag() {
        tag01:
        for (int i = 0; i < 10; i++) {
            tag02:
            for (int j = 0; j < 10; j++) {
                if (j == 5) {
                    continue tag02;
                }
                System.out.println("i: " + (i+1) + " , j:" + (j + 1));
            }
            System.out.println("message");
        }
    }


}
