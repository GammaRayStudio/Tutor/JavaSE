package com.enoxs.se;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class ObjectEquals {


    @Test
    public void compareInt(){
        int num01 = 10;
        int num02 = 10;
        System.out.println(num01 == num02);
    }

    @Test
    public void compareObj(){
        BigDecimal num01 = new BigDecimal(10);
        BigDecimal num02 = new BigDecimal(10);
        System.out.println(num01 == num02);
        System.out.println(num01.equals(num02));
    }

}
