package com.enoxs.service;

import com.enoxs.defdata.AppInfo;
import com.enoxs.service.impl.AppServiceImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AppServiceTest {

    AppService appService = new AppServiceImpl();

    @Test
    void isLastlyVersion() {
        AppInfo appInfo = new AppInfo();
        appInfo.setId(Long.valueOf(1));
        appInfo.setName("JavaProjSE");
        appInfo.setVersion("1.0.3");
        appInfo.setAuthor("Enoxs");
        appInfo.setRemark("Java Project - Sample Example");

        boolean isLastlyVersion = appService.isLastlyVersion("1.0.3" , appInfo);
        assertTrue(isLastlyVersion);

    }

    @Test
    void runConstructor(){
        AppInfo appInfo = new AppInfo(Long.valueOf(2) , "JUnitSE" , "1.0.2");
    }
}