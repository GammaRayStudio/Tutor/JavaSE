package com.enoxs.service;

import com.enoxs.defdata.AppInfo;

public interface AppService {
    boolean isLastlyVersion(String version, AppInfo appInfo);
}
