package com.enoxs.se;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class BigDecimalMath {

    @Test
    public void calFloat01(){
        double num = 0.1 + 0.1 + 0.1;
        System.out.println(num);
    }

    @Test
    public void calFloat02(){
        double num = 1.0 - 0.8;
        System.out.println(num);
    }


    @Test
    public void checkCalFloat(){
        double num = 1.0 - 0.8;
        System.out.println(num);

        if(num == 0.2) {
            System.out.println("number is 0.2");
        }else{
            System.out.println("number is not 0.2");
        }
    }


    @Test
    public void calBigDecimal(){
        BigDecimal num = BigDecimal.valueOf(0.1);
        BigDecimal big = BigDecimal.valueOf(0);
        big = big.add(num).add(num).add(num);

        System.out.println(big);

        BigDecimal expect = BigDecimal.valueOf(0.3);

        if(big.equals(expect)) {
            System.out.println("number is 0.3");
        }else{
            System.out.println("number is not 0.3");
        }
    }

}
