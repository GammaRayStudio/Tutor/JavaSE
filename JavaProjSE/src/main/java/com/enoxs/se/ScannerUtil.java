package com.enoxs.se;

import java.util.Scanner;

public class ScannerUtil {

    public ScannerUtil(){
        Scanner input = new Scanner(System.in);
        System.out.println("Please input 3 number : ");
        int num01 = input.nextInt();
        int num02 = input.nextInt();
        int num03 = input.nextInt();
        System.out.println("number 01 => " + num01);
        System.out.println("number 02 => " + num02);
        System.out.println("number 03 => " + num03);
        System.out.println("Sum() = > " + (num01 + num02 + num03));
    }

    public static void main(String[] args) {
        new ScannerUtil();
    }

}
