package com.enoxs.se;

import org.junit.jupiter.api.Test;

public class VarTypeWrap {

    @Test
    public void runInteger(){
        int num01 = 10;
        int num02 = 20;

        Integer wrap01 = new Integer(num01);
        Integer wrap02 = new Integer(num02);

        System.out.println(num01 / 3);
        System.out.println(wrap01.doubleValue() / 3);
        System.out.println(wrap01.compareTo(wrap02));
    }

    @Test
    public void runAutoBoxing(){
        int num = 10;
        Integer wrap01 = new Integer(num);
        Integer wrap02 = num;
        System.out.println(wrap01);
        System.out.println(wrap02);
    }

    @Test
    public void runAutoUnboxing(){
        int num01 = 10;
        Integer wrap = new Integer(num01);
        int num02 = wrap;
        System.out.println(num02);
    }

}
