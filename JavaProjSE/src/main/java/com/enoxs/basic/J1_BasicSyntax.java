package com.enoxs.basic;

import org.junit.jupiter.api.Test;

import java.nio.charset.StandardCharsets;

public class J1_BasicSyntax {

    @Test
    public void printByte(){
        byte b = 0x41;
        System.out.printf("十六進制 : %x %n" , b); // 0x41
        System.out.printf("十進制 : %d %n" , b); // 65
        System.out.printf("字元 : %c %n" , b); // A
    }

    @Test
    public void varBoolean(){
        boolean isTrue = true;
        boolean isFalse = false;
        boolean isFlag = false;

        if(isTrue){
            System.out.println("isTrue ? => Yes");
        }else{
            System.out.println("isTrue ? => No");
        }

        if(isFalse){
            System.out.println("isFalse ? => No");
        }else{
            System.out.println("isFalse ? => Yes");
        }

        isFlag = true;

        if(isFlag){
            System.out.println("Status => ON");
        }else{
            System.out.println("Status => OFF");
        }
    }


    @Test
    public void varNumber(){
        short numShort = 100;
        int numInt = 100; // <- 預設
        long numLong = 100;

        short sum01 = numShort;
        int sum02 = numShort + numInt + 50;
        long sum03 = numShort + numInt + numLong;

        System.out.println("Number : ");
        System.out.println("num01 => " + sum01);
        System.out.println("num02 => " + sum02);
        System.out.println("num03 => " + sum03);

    }

    @Test
    public void varFloat(){
        float numFloat = (float) 100.200;
        double numDouble = 100.200; // <-預設
        System.out.println("Number : ");
        System.out.println("numFloat => " + numFloat);
        System.out.println("numDouble => " + numDouble);
    }

    @Test
    public void varChar(){
        char a = 'A';
        char b = 'B';
        char c = '林';

        System.out.println("a => " + a);
        System.out.println("b => " + b);
        System.out.println("c => " + c);

        int num = (int)a;

        System.out.println("十進制 : " + num);

        System.out.printf("十六進制 : %x %n" , num);

    }

    @Test
    public void rangeTest(){
        // byte、short、int、long 範圍
        System.out.printf("Byte : %d ~ %d%n",
                Byte.MIN_VALUE, Byte.MAX_VALUE);
        System.out.printf("Short : %d ~ %d%n",
                Short.MIN_VALUE, Short.MAX_VALUE);
        System.out.printf("Integer : %d ~ %d%n",
                Integer.MIN_VALUE, Integer.MAX_VALUE);
        System.out.printf("Long : %d ~ %d%n",
                Long.MIN_VALUE, Long.MAX_VALUE);
        // float、double 精度範圍
        System.out.printf("Float : %d ~ %d%n",
                Float.MIN_EXPONENT, Float.MAX_EXPONENT);
        System.out.printf("Double : %d ~ %d%n",
                Double.MIN_EXPONENT, Double.MAX_EXPONENT);
        // char 可表示的 Unicode 範圍
        System.out.printf("Character : %h ~ %h%n",
                Character.MIN_VALUE, Character.MAX_VALUE);
        // boolean 的兩個值
        System.out.printf("Boolean : %b ~ %b%n",
                Boolean.TRUE, Boolean.FALSE);
    }


    int version = 1;

    @Test
    public void varArea(){
        int number = 2;
        System.out.println("version => " + version);
        System.out.println("number => " + number);
    }

    @Test
    public void runArithmetic(){
        int a = 10;
        int b = 20;
        int c = 25;
        int d = 25;

        System.out.println("a + b = " + (a + b) );
        System.out.println("a - b = " + (a - b) );
        System.out.println("a * b = " + (a * b) );
        System.out.println("b / a = " + (b / a) );
        System.out.println("b % a = " + (b % a) );
        System.out.println("c % a = " + (c % a) );
        System.out.println("a++   = " +  (a++) );
        System.out.println("a-- = " + (a--) );

        // Check the difference in d++ and ++d
        System.out.println("d++   = " +  (d++) );
        System.out.println("++d   = " +  (++d) );
    }

    @Test
    public void runIncrement(){
        int index = 0;
        System.out.println(++index);
        System.out.println(index);
        System.out.println(index++);
        System.out.println(index);
    }

    @Test
    public void runDecrement(){
        int index = 3;
        System.out.println(--index);
        System.out.println(index);
        System.out.println(index--);
        System.out.println(index);
    }


    @Test
    public void runReleational(){
        int a = 10;
        int b = 20;

        System.out.println("a == b : " + (a == b) );
        System.out.println("a != b : " + (a != b) );
        System.out.println("a > b : " + (a > b) );
        System.out.println("a < b : " + (a < b) );
        System.out.println("b >= a : " + (b >= a) );
        System.out.println("b <= a : " + (b <= a) );
    }

    @Test
    public void runBitwise01(){
        int a = 60;	/* 60 = 0011 1100 */
        int b = 13;	/* 13 = 0000 1101 */
        int c = 0;

        c = a & b;        /* 12 = 0000 1100 */
        System.out.println("a & b = " + c );

        c = a | b;        /* 61 = 0011 1101 */
        System.out.println("a | b = " + c );

        c = a ^ b;        /* 49 = 0011 0001 */
        System.out.println("a ^ b = " + c );

        c = ~a;           /*-61 = 1100 0011 */
        System.out.println("~a = " + c );

        c = a << 2;       /* 240 = 1111 0000 */
        System.out.println("a << 2 = " + c );

        c = a >> 2;       /* 15 = 1111 */
        System.out.println("a >> 2  = " + c );

        c = a >>> 2;      /* 15 = 0000 1111 */
        System.out.println("a >>> 2 = " + c );
    }

    @Test
    public void runBitWise02(){
        System.out.println("AND運算：");
        System.out.printf("0 AND 0 %5d%n", 0 & 1);
        System.out.printf("0 AND 1 %5d%n", 0 & 1);
        System.out.printf("1 AND 0 %5d%n", 1 & 0);
        System.out.printf("1 AND 1 %5d%n", 1 & 1);

        System.out.println("\nOR運算：");
        System.out.printf("0 OR 0 %6d%n", 0 | 0);
        System.out.printf("0 OR 1 %6d%n", 0 | 1);
        System.out.printf("1 OR 0 %6d%n", 1 | 0);
        System.out.printf("1 OR 1 %6d%n", 1 | 1);

        System.out.println("\nXOR運算：");
        System.out.printf("0 XOR 0 %5d%n", 0 ^ 0);
        System.out.printf("0 XOR 1 %5d%n", 0 ^ 1);
        System.out.printf("1 XOR 0 %5d%n", 1 ^ 0);
        System.out.printf("1 XOR 1 %5d%n", 1 ^ 1);
    }

    @Test
    public void runBitWise03(){
        int number = 1;
        System.out.printf( "2 的 0 次方: %d%n", number);
        System.out.printf( "2 的 1 次方: %d%n", number << 1);
        System.out.printf( "2 的 2 次方: %d%n", number << 2);
        System.out.printf( "2 的 3 次方: %d%n", number << 3);
    }

    @Test
    public void runLogical(){
        boolean a = true;
        boolean b = false;

        System.out.println("a && b = " + (a&&b));
        System.out.println("a || b = " + (a||b) );
        System.out.println("!(a && b) = " + !(a && b));
    }



    @Test
    public void runAssignment(){
        int a = 10;
        int b = 20;
        int c = 0;

        c = a + b;
        System.out.println("c = a + b , c = " + c );

        c += a ;
        System.out.println("c += a  , c = " + c );

        c -= a ;
        System.out.println("c -= a , c = " + c );

        c *= a ;
        System.out.println("c *= a , c = " + c );

        a = 10;
        c = 15;
        c /= a ;
        System.out.println("c /= a , c = " + c );

        a = 10;
        c = 15;
        c %= a ;
        System.out.println("c %= a  , c = " + c );

        c <<= 2 ;
        System.out.println("c <<= 2 , c = " + c );

        c >>= 2 ;
        System.out.println("c >>= 2 , c = " + c );

        c >>= 2 ;
        System.out.println("c >>= 2 , c = " + c );

        c &= a ;
        System.out.println("c &= a  , c = " + c );

        c ^= a ;
        System.out.println("c ^= a   , c = " + c );

        c |= a ;
        System.out.println("c |= a   , c = " + c );

    }

    @Test
    public void runConditional(){
        int score = 100;
        boolean isLargeThenSixty = score > 60 ? true : false;
        System.out.printf("isLargeThenSixty = %b%n" , isLargeThenSixty);
    }

}
