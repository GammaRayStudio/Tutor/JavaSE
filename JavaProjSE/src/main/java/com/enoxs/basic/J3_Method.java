package com.enoxs.basic;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class J3_Method {


    public void printMsg(){
        System.out.println("Message");
    }

    public int calSum(int a , int b){
        return a + b;
    }

    public boolean isLargeThanTen(int num){
        return num > 10 ? true : false;
    }

    @Test
    public void testPrintMsg(){
        printMsg();
    }

    @Test
    public void testCalSum(){
        int a = 10;
        int b = 20;
        int c = 0;
        c = calSum(a , b);
        System.out.println("a + b = " + c);
    }

    @Test
    public void testIsLargeThen(){
        int num = 15;

        boolean isLarge = isLargeThanTen(num);

        System.out.println("number > 10 ? => " + isLarge);
    }


    @Test
    public void testSucc(){
        int a = 10;
        int b = 20;
        int actual = calSum(a,b);
        int expect = 30;
        assertEquals(expect,actual);
    }

    @Test
    public void testFail(){
        int a = 10;
        int b = 20;
        int actual = calSum(a,b);
        int expect = 20;
        assertEquals(expect,actual);
    }


    public int calTotal(int ... num){
        int result = 0;

        for(int i=0;i<num.length;i++){
            result += num[i];
            // result = result + num[i]
        }

        return result;
    }

    @Test
    public void testCalTotal(){
        int [] number = {1,2,3,4,5,6,7,8,9,10};
        int result = calTotal(number);
        System.out.println("result => " + result);
    }

    public int calHalfLess50(int number){
        int result = number / 2;
        if (result > 50){
            result = calHalfLess50(result);
        }
        return result;
    }

    @Test
    public void testCalHalfTotal(){
        int num = 120;
        int result = calHalfLess50(num);
        System.out.println("result => " + result);
    }

    public int calGCD(int a , int b){
        int result = 0;
        if(a % b == 0){
            result = b;
        }else{
            result = calGCD(b , a%b);
        }
        return result;
    }

    @Test
    public void testCalGCD(){
        int num01 = 50;
        int num02 = 100;
        int result = calGCD(num01 , num02);
        System.out.println("result => " + result);
    }


}
