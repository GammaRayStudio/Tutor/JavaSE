package com.enoxs.se;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

public class ArrayObject {

    @Test
    public void runDeclareArray() {
        int[] num01 = new int[5];

        num01[0] = 1;
        num01[1] = 2;
        num01[2] = 3;
        num01[3] = 4;
        num01[4] = 5;

        System.out.println("index : 0 => " + num01[0]);
        for (int n : num01) {
            System.out.print(n);
            System.out.print(" ");
        }

        System.out.println();

        int[] num02 = {1, 2, 3, 4, 5};

        for (int n : num02) {
            System.out.print(n);
            System.out.print(" ");
        }

    }

    @Test
    public void runDeclareTwoArray() {
        int[][] num01 = new int[2][3];

        for (int i = 0; i < num01.length; i++) {
            for (int j = 0; j < num01[i].length; j++) {
                num01[i][j] = (i + 1) * (j + 1);
            }
        }


        for (int[] arr : num01) {
            for (int n : arr) {
                System.out.print(n);
                System.out.print(" ");
            }
            System.out.println();
        }

        System.out.println();

        int[][] num02 = {
                {1, 2, 3},
                {4, 5, 6}
        };

        for (int[] arr : num02) {
            for (int n : arr) {
                System.out.print(n);
                System.out.print(" ");
            }
            System.out.println();
        }
    }

    @Test
    public void runDeclareThreeArray() {
        int[][][] num01 = new int[2][3][3];

        for (int i = 0; i < num01.length; i++) {
            for (int j = 0; j < num01[i].length; j++) {
                for (int k = 0; k < num01[i][j].length; k++) {
                    num01[i][j][k] = (i + 1) * (j + 1) * (k + 1);
                    System.out.print(num01[i][j][k]);
                    System.out.print(" ");
                }
                System.out.println();
            }
            System.out.println();
        }

        int[][][] num02 = {
                {
                        {1, 2, 3},
                        {4, 5, 6},
                        {7, 8, 9}
                },
                {
                        {10, 11, 12},
                        {13, 14, 15},
                        {16, 17, 18}
                }
        };

        for(int [][] binArr : num02){
            for(int [] arr : binArr ){
                for(int n : arr){
                    System.out.print(n);
                    System.out.print(" ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }

    @Test
    public void runArrayFunc(){
        int [] num = new int [10];
        System.out.println(Arrays.toString(num));
        Arrays.fill(num , 100);
        System.out.println(Arrays.toString(num));
    }

    @Test
    public void runArrayCopy(){
        int [] num = new int [10];
        System.out.println(" origin data");
        Arrays.fill(num , 100);
        System.out.println(Arrays.toString(num));

        System.out.println("\n");
        System.out.println(" arr = num ");
        int [] arr = num;
        System.out.println("num :");
        System.out.println(Arrays.toString(num));

        System.out.println("arr :");
        System.out.println(Arrays.toString(arr));

        System.out.println();

        num[0] = 0;
        System.out.println("num :");
        System.out.println(Arrays.toString(num));

        System.out.println("arr :");
        System.out.println(Arrays.toString(arr));

        System.out.println("\n");

        System.out.println(" clone = Arrays.copyOf(num , len)");
        Arrays.fill(num , 100);
        int [] clone = Arrays.copyOf(num , num.length);
        System.out.println("num :");
        System.out.println(Arrays.toString(num));

        System.out.println("clone :");
        System.out.println(Arrays.toString(clone));

        System.out.println();

        num[0] = 0;
        System.out.println("num :");
        System.out.println(Arrays.toString(num));

        System.out.println("clone :");
        System.out.println(Arrays.toString(clone));

    }

}
