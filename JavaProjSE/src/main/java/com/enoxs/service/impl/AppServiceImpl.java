package com.enoxs.service.impl;

import com.enoxs.defdata.AppInfo;
import com.enoxs.service.AppService;

public class AppServiceImpl implements AppService {

    @Override
    public boolean isLastlyVersion(String version, AppInfo appInfo) {
        return version.equals(appInfo.getVersion()) ? true : false;
    }

}
